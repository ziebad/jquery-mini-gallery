$(document).ready(function () {

    // Array with images src on the list
    var src_array = $(".images img").map(function () {
        return $(this).attr("src");
    }).get();

    // Width of one thumbnails
    const width = $('.images li img').css("width");
    // Number of images on the list
    const total = src_array.length;
    // Number of thumbnails, that are shown on the slider
    const sizeSlider = parseInt(parseInt($('#slider').css("width")) / parseInt(width));

    // Keep previous image index
    var prevImg = 0;
    // Current image index
    var current = 0;
    // Keep image index that is displayed on the slider as the last
    var sliderPosition = sizeSlider;

    $('#mainImg').attr('src', src_array[0]);
    $('#leftSlider').css("visibility", "hidden");

    $('.images li img').click(function () {
        var imageScr = $(this).attr('src');
        prevImg = current;
        current = $(this).parent('.image').index();
        if (prevImg != current) {
            $('.images li').eq(prevImg).removeClass('current');
            $('.images li').eq(current).addClass('current');
        }
        $('#mainImg').fadeOut(150, function () {
            $('#mainImg').attr('src', imageScr);
        }).fadeIn(150);
    });

    $('#next').click(function () {
        $('#mainImg').fadeOut(150, function () {
            prevImg = current;
            current += 1;
            if (current == total) {
                current = 0;
                sliderPosition = sizeSlider;
                $('#slider .images').animate({'margin-left': '0px'}, 1000);
                $('#leftSlider').css("visibility", "hidden");
                if ($('#rightSlider').css("visibility") == "hidden") {
                    $('#rightSlider').css("visibility", "visible");
                }
            } else {
                if (current - 1 >= sliderPosition - sizeSlider && current <= sliderPosition) {
                    if (sliderPosition < total) {
                        sliderPosition += 1;
                        $('#slider .images').animate({'margin-left': '-=' + width}, 150);
                    }
                } else {
                    var newMarginLeft;
                    if (current >= total - sizeSlider) {
                        newMarginLeft = parseInt(width) * (total - sizeSlider) * -1 + 'px';
                        sliderPosition = total;
                    } else {
                        newMarginLeft = parseInt(width) * (current - 1) * -1 + 'px';
                        sliderPosition = current + 2;
                    }
                    $('#slider .images').animate({'margin-left': newMarginLeft}, 1000);
                }
                if (sliderPosition == total) {
                    $('#rightSlider').css("visibility", "hidden");
                } else if ($('#rightSlider').css("visibility") == "hidden") {
                    $('#rightSlider').css("visibility", "visible");
                }
                if ($('#leftSlider').css("visibility") == "hidden" && sliderPosition > sizeSlider) {
                    $('#leftSlider').css("visibility", "visible");
                }
            }
            if (prevImg != current) {
                $('.images li').eq(prevImg).removeClass('current');
                $('.images li').eq(current).addClass('current');
            }
            $('#mainImg').attr('src', src_array[current]);
        }).fadeIn(150);
    });

    $('#prev').click(function () {
        $('#mainImg').fadeOut(150, function () {
            prevImg = current;
            current -= 1;
            if (current < 0) {
                current = total - 1;
                var newMarginLeft = parseInt(width) * (total - sizeSlider) * -1 + 'px';
                $('#slider .images').animate({'margin-left': newMarginLeft}, 1000);
                sliderPosition = total;
                $('#rightSlider').css("visibility", "hidden");
                if ($('#leftSlider').css("visibility") == "hidden") {
                    $('#leftSlider').css("visibility", "visible");
                }
            } else {
                if (current >= sliderPosition - sizeSlider && current < sliderPosition - 1) {
                    if (current + 1 >= sizeSlider) {
                        $('#slider .images').animate({'margin-left': '+=' + width}, 150);
                        sliderPosition -= 1;
                    }
                } else {
                    var newMarginLeft = (current >= 0 && current < sizeSlider - 1 ? 0 : parseInt(width) * (current - 2) * -1) + 'px';
                    sliderPosition = current < sizeSlider ? sizeSlider : current + 1;
                    $('#slider .images').animate({'margin-left': newMarginLeft}, 1000);
                }
                if (sliderPosition <= sizeSlider) {
                    $('#leftSlider').css("visibility", "hidden");
                } else if ($('#leftSlider').css("visibility") == "hidden") {
                    $('#leftSlider').css("visibility", "visible");
                }
                if ($('#rightSlider').css("visibility") == "hidden") {
                    $('#rightSlider').css("visibility", "visible");
                }
            }
            if (prevImg != current) {
                $('.images li').eq(prevImg).removeClass('current');
                $('.images li').eq(current).addClass('current');
            }
            $('#mainImg').attr('src', src_array[current]);
        }).fadeIn(150);
    });

    $('#rightSlider').click(function () {
        sliderPosition += 1;
        $('#slider .images').animate({'margin-left': '-=' + width}, 150);
        if (sliderPosition == total) {
            $('#rightSlider').css("visibility", "hidden");
        }
        if ($('#leftSlider').css("visibility") == "hidden") {
            $('#leftSlider').css("visibility", "visible");
        }
    });

    $('#leftSlider').click(function () {
        sliderPosition -= 1;
        $('#slider .images').animate({'margin-left': '+=' + width}, 150);
        if (sliderPosition == sizeSlider) {
            $('#leftSlider').css("visibility", "hidden");
        }
        if ($('#rightSlider').css("visibility") == "hidden") {
            $('#rightSlider').css("visibility", "visible");
        }
    });
});